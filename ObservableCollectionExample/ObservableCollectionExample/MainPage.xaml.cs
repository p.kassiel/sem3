﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409
using ObservableCollectionExample.Models;
using System.Collections.ObjectModel;
namespace ObservableCollectionExample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private List<Icon> Icons;
        private ObservableCollection<contact> Contacts;
        public MainPage()
        {
            this.InitializeComponent();
            Icons = new List<Icon>();
            Icons.Add(new Icon { IconPath = "Asset/images/male-01.png" });
            Icons.Add(new Icon { IconPath = "Asset/images/male-02.png" });
            Icons.Add(new Icon { IconPath = "Asset/images/male-03.png" });
            Icons.Add(new Icon { IconPath = "Asset/images/female-01.png" });
            Icons.Add(new Icon { IconPath = "Asset/images/female-02.png" });
            Icons.Add(new Icon { IconPath = "Asset/images/female-03.png" });
            Contacts = new ObservableCollection<contact>();
        }
        private void NewContactbutton_Click(object sender,RoutedEventArgs e)
        {
            string avatar = ((Icon)AvarComboBox.SelectedValue).IconPath;
            Contacts.Add(new contact { FirstName = FirstNameTexBox.Text, LastName = LastNameTextBox.Text, AvatarPath = avatar });
            FirstNameTexBox.Text = "";
            LastNameTextBox.Text = "";
            AvarComboBox.SelectedIndex = -1;
            FirstNameTexBox.Focus(FocusState.Programmatic);
        }
    }
}
