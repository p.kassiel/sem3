﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2.models
{
    public class book
    {
        public int bookID { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public string coverimage { get; set; }
    }
    public class bookmanager
    {
        public static List<book> GetBooks()
        {
            var books = new List<book>();

            books.Add(new book { bookID = 1, title = "vulpate", author = "futurum", coverimage = "Assets/image/1.png" });
            books.Add(new book { bookID = 2, title = "Mazim", author = "Sequiter Que", coverimage = "Assets/image/2.png" });
            books.Add(new book { bookID = 3, title = "Elit", author = "Tempor", coverimage = "Assets/image/3.png" });
            books.Add(new book { bookID = 4, title = "etiam", author = "Option", coverimage = "Assets/image/4.png" });
            books.Add(new book { bookID = 5, title = "p5", author = "Accumsam", coverimage = "Assets/image/5.png" });
            books.Add(new book { bookID = 6, title = "p6", author = "Legunt Xaepius", coverimage = "Assets/image/6.png" });
            books.Add(new book { bookID = 7, title = "p7", author = "Eleifend", coverimage = "Assets/image/7.png" });
            books.Add(new book { bookID = 8, title = "p8", author = "Vero Tation", coverimage = "Assets/image/8.png" });
            books.Add(new book { bookID = 9, title = "p9", author = "Jack Tibbles", coverimage = "Assets/image/9.png" });
            books.Add(new book { bookID = 10, title = "p10", author = "Tufy Tibbles", coverimage = "Assets/image/10.png" });
            books.Add(new book { bookID = 11, title = "p11", author = "volupat", coverimage = "Assets/image/11.png" });
            books.Add(new book { bookID = 12, title = "p12", author = "Est posim", coverimage = "Assets/image/12.png" });
            books.Add(new book { bookID = 13, title = "p13", author = "Magna", coverimage = "Assets/image/13.png" });
            return books;
        }
    }
}

